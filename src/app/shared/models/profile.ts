export interface Profile {
    username?: string;
    photo?: string;
    auth_token?: string;
}