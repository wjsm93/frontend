import { Profile } from './../models/profile';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class Api {

  public APIUrl: string;
  serviceHeader: any;
  serviceOptions: any;

  private profileData = new Subject<any>();

  constructor(public http: Http) {

    this.APIUrl = "https://taiga.tecnoandina.cl/api/v1/";

    this.serviceHeader = new Headers({
      'Content-Type': 'application/json'
    });

    this.serviceOptions = new RequestOptions({
      headers: this.serviceHeader,
      method: 'post'
    });
  }

  publishRefresh(data){
    console.log('publishRefresh', data);
    this.profileData.next(data);
  }

  getProfileData(): Observable<any> {
    console.log('help!');
    return this.profileData.asObservable();
  }

  get(url:string, params:Object = null) {
    var urlMethod;
    if (params != null) {
      var requestparams = '';
      var length = Object.keys(params).length;
      var counter = 1;
      for (var key in params) {
        if (counter == length) {
          requestparams += key + "=" + params[key];
        } else {
          requestparams += key + "=" + params[key] + "&";
        }
        counter++
      }
      urlMethod = this.APIUrl + url + "?" + requestparams;
    } else {
      urlMethod = this.APIUrl + url;
    }

    let serviceHeader = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem("token")
    });

    let serviceOptions = new RequestOptions({
      headers: serviceHeader
    });

    return this.http.get(urlMethod, serviceOptions).pipe(map(res => res.json()));
  }

  post(url:string, body:Object) {
    var url = this.APIUrl + url;
    return this.http.post(url, body, this.serviceOptions).pipe(map(res => res.json()));
  }

}