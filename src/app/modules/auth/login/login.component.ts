import { Api } from './../../../shared/services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form: any;

  constructor(
    private api: Api,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      mail: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  login() {
    const body = {
      type: 'normal',
      username: this.form.get('mail').value,
      password: this.form.get('password').value
    }
    this.api.post('auth', body).subscribe(
      data => {
        console.log("data login", data);
        sessionStorage.setItem("token", data.auth_token);
        this.api.publishRefresh(data);
      },
      error => {
        alert(error);
      }
    );
  }

}
