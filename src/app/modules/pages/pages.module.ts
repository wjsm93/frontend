import { ListComponent } from './list/list.component';
import { PagesRoutingModule } from './pages-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    PagesRoutingModule,
    CommonModule
  ],
  providers: [],
  bootstrap: []
})
export class PagesModule { }
