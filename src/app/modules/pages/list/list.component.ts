import { Api } from './../../../shared/services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  userstories;

  constructor(
    private api: Api
  ) { }

  ngOnInit() {
    this.getProject();
  }

  getProject() {
    this.api.get('userstories', { project: 46 }).subscribe(data => {
      this.userstories = data;
      console.log(data);
    });
  }

}
