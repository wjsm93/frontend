import { Profile } from './shared/models/profile';
import { Api } from './shared/services/api.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  public isLogged: boolean = false;
  subscription: Subscription;
  public profile: Profile;

  constructor(
    private router: Router,
    private api: Api
  ) {
    
  }

  ngOnInit() {
    this.verifySession();
    this.subscription = this.api.getProfileData().subscribe(data => {
      console.log('getProfileData');
      if (data) {
        console.log('perfil', data);
        this.isLogged = true;
        this.profile = data;
        this.router.navigate(['/pages/list']);
      }
    });
  }

  verifySession() {
    if (!this.isLogged) {
      this.router.navigate(['/auth/login']);
    }
  }

}